<?php
if (!defined ("TYPO3_MODE")) 	die ("Access denied.");

$TCA["tx_gbglossary_domain_model_definition"] = Array (
  "ctrl" => $TCA["tx_gbglossary_domain_model_definition"]["ctrl"],
  "interface" => Array (
    "showRecordFieldList" => "sys_language_uid,l18n_parent,l18n_diffsource,hidden,starttime,endtime,fe_group,short,shortcut,longversion,shorttype,description,exclude, website"
  ),
  "feInterface" => $TCA["tx_gbglossary_domain_model_definition"]["feInterface"],
  "columns" => Array (
    'sys_language_uid' => Array (
      'exclude' => 1,
      'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.language',
      'config' => Array (
        'type' => 'select',
        'foreign_table' => 'sys_language',
        'foreign_table_where' => 'ORDER BY sys_language.title',
        'items' => Array(
          Array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages',-1),
          Array('LLL:EXT:lang/locallang_general.xml:LGL.default_value',0)
        )
      )
    ),
    'l18n_parent' => Array (
      'displayCond' => 'FIELD:sys_language_uid:>:0',
      'exclude' => 1,
      'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.l18n_parent',
      'config' => Array (
        'type' => 'select',
        'items' => Array (
          Array('', 0),
        ),
        'foreign_table' => 'tx_gbglossary_domain_model_definition',
        'foreign_table_where' => 'AND tx_gbglossary_domain_model_definition.pid=###CURRENT_PID### AND tx_gbglossary_domain_model_definition.sys_language_uid IN (-1,0)',
      )
    ),
    'l18n_diffsource' => Array (
      'config' => Array (
        'type' => 'passthrough'
      )
    ),
    "hidden" => Array (
      "exclude" => 1,
      "label" => "LLL:EXT:lang/locallang_general.xml:LGL.hidden",
      "config" => Array (
        "type" => "check",
        "default" => "0"
      )
    ),
    "starttime" => Array (
      "exclude" => 1,
      "label" => "LLL:EXT:lang/locallang_general.xml:LGL.starttime",
      "config" => Array (
        "type" => "input",
        "size" => "8",
        "max" => "20",
        "eval" => "date",
        "default" => "0",
        "checkbox" => "0"
      )
    ),
    "endtime" => Array (
      "exclude" => 1,
      "label" => "LLL:EXT:lang/locallang_general.xml:LGL.endtime",
      "config" => Array (
        "type" => "input",
        "size" => "8",
        "max" => "20",
        "eval" => "date",
        "checkbox" => "0",
        "default" => "0",
        "range" => Array (
          "upper" => mktime(0,0,0,12,31,2037),
          "lower" => mktime(0,0,0,date("m")-1,date("d"),date("Y"))
        )
      )
    ),
    "fe_group" => Array (
      "exclude" => 1,
      "label" => "LLL:EXT:lang/locallang_general.xml:LGL.fe_group",
      "config" => Array (
        "type" => "select",
        "items" => Array (
          Array("", 0),
          Array("LLL:EXT:lang/locallang_general.xml:LGL.hide_at_login", -1),
          Array("LLL:EXT:lang/locallang_general.xml:LGL.any_login", -2),
          Array("LLL:EXT:lang/locallang_general.xml:LGL.usergroups", "--div--")
        ),
        "foreign_table" => "fe_groups"
      )
    ),
    "short" => Array (
      "exclude" => 1,
      "label" => "LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.short",
      "config" => Array (
        "type" => "input",
        "size" => "30",
        "eval" => "required",
      )
    ),
    "shortcut" => Array (
      "exclude" => 1,
      "label" => "LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.shortcut",
      "config" => Array (
        "type" => "input",
        "size" => "30",
      )
    ),
    "longversion" => Array (
      "exclude" => 1,
      "label" => "LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.longversion",
      "config" => Array (
        "type" => "input",
        "size" => "48",
      )
    ),
    "shorttype" => Array (
      "exclude" => 1,
      "label" => "LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.shorttype",
      "config" => Array (
        "type" => "select",
        "items" => Array (
          Array("LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.shorttype.I.0", "dfn"),
          Array("LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.shorttype.I.1", "acronym"),
          Array("LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.shorttype.I.2", "abbr"),
        ),
        "size" => 1,
        "maxitems" => 1,
      )
    ),
    "description" => Array (
      "exclude" => 1,
      "label" => "LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.description",
      "config" => Array (
        "type" => "text",
        "cols" => "48",
        "rows" => "5",
        "wizards" => Array(
          "_PADDING" => 2,
          "RTE" => Array(
            "notNewRecords" => 1,
            "RTEonly" => 1,
            "type" => "script",
            "title" => "Full screen Rich Text Editing|Formatteret redigering i hele vinduet",
            "icon" => "wizard_rte2.gif",
            "script" => "wizard_rte.php",
          ),
        ),
      )
    ),
    "website" => Array (
      "exclude" => 0,
      "label" => "LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.website",
      "config" => Array (
        "type" => "input",
        "size" => "48",
        "max" => "255",
        "checkbox" => "",
        "eval" => "trim",
        "wizards" => Array(
          "_PADDING" => 2,
          "link" => Array(
            "type" => "popup",
            "title" => "Link",
            "icon" => "link_popup.gif",
            "script" => "browse_links.php?mode=wizard",
            "JSopenParams" => "height=300,width=500,status=0,menubar=0,scrollbars=1"
          )
        )
      )
    ),
    "exclude" => Array (
      "exclude" => 1,
      "label" => "LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition.exclude",
      "config" => Array (
        "type" => "check",
      )
    ),
  ),
  "types" => Array (
    "0" => Array("showitem" => "sys_language_uid;;;;1-1-1, l18n_parent, l18n_diffsource, hidden;;1, short, shortcut, longversion, shorttype, description;;;richtext[cut|copy|paste|formatblock|textcolor|bold|italic|underline|left|center|right|orderedlist|unorderedlist|outdent|indent|link|table|image|line|chMode]:rte_transform[mode=ts_css|imgpath=uploads/tx_gbglossary/rte/], link, exclude, website")
  ),
  "palettes" => Array (
    "1" => Array("showitem" => "starttime, endtime, fe_group"),
    "2" => Array("showitem" => "shortcut"),
  )
);
