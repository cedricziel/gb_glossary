<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2009-2012 Gute Botschafter Gmbh, Morton Jonuschat <m.jonuschat@gute-botschafter.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
/**
 * The Glossary controller for the gb_glossary package
 *
 * @author Morton Jonuschat <m.jonuschat@gute-botschafter.de>
 * @copyright Copyright (c) 2009-2012, Gute Botschafter Gmbh, Morton Jonuschat
 * @version $Id:$
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */
class User_Tx_GbGlossary_Markup
{
  /**
   * TYPO3 cObject
   *
   * @var object
   */
  public $cObj = null;
  /**
   * Word Replacement List
   *
   * @var array
   */
  protected $glossaryEntries = array();
  /**
   * Word Replacement List Index
   *
   * @var array
   */
  protected $glossaryIndex = array();
  /**
   * Configuration Information
   *
   * @param array
   */
  protected $config = null;
  /**
   * Constructor for User_Tx_GbGlossary_Markup
   *
   * @access public
   * @return void
   */
  public function __construct ()
  {
    $this->cObj = t3lib_div::makeInstance('tslib_cObj');
    if (! is_object($this->cObj)) {
      throw new Exception('Creating TYPO3 cObject failed!', 500);
    }
  }
  /**
   * public function to mark the glossary entries in the html fragment
   *
   * @param string $content
   * @param array $config
   */
  public function markGlossaryWords ($content = '', $config = '')
  {
    // initialize plugin configuration
    if (null === $this->config)
      $this->config = $config;
    // Skip excludes pages
    if (t3lib_div::inList($this->config['excludePages'], $GLOBALS['TSFE']->id) || $GLOBALS['TSFE']->id == $this->config['detailsPid']) {
      return $content;
    }
    // retrieve the glossary words from database
    $this->getGlossaryWords();
    // create a new DOM object
    $domObject = new DOMDocument();
    // wrap in an extra div to have a known starting point
    $htmlCode  = '<html><head><meta http-equiv="Content-Type" content="text/html; charset='.mb_detect_encoding($content).'"></head><body><div>';
    $htmlCode .= $content;
    $htmlCode .= '</div></body></html>';
    $domObject->loadHTML($htmlCode);
    // set the root to the outermost div we added
    $root = $domObject->getElementsByTagName('div')->item(0);
    // walk the DOM tree
    $this->walkDom($root);
    // create the new content with rendered tooltips
    $content = $this->createToolTips($domObject, $root, mb_detect_encoding($content));
    $this->fixJavascript($content);
    $this->fixCheckTagTypeCounts($content);
    return $content;
  }
  /**
   * Replace "broken" Javascript declarations produced by libxml which lead to parsing errors in misc. browsers
   *
   * @param &$content HTML Content after being run through libxml
   * @return void
   */
  protected function fixJavascript(&$content = '') {
    $search = array(
      '<script type="text/javascript"><![CDATA[',
      ']]></script>'
    );
    $replace = array(
      '<script type="text/javascript">/*<![CDATA[*/',
      '/*]]>*/</script>'
    );
    $content = str_replace($search, $replace, $content);
  }
  /**
   * Work around a incompleteness in t3lib_parsehtml::checkTagCount which doesn't work with <br></br>
   *
   * @param &$content HTML Content after being run through libxml
   * @return void
   */
  protected function fixCheckTagTypeCounts(&$content = '') {
    $search = array(
      '<p/>',
      '<br></br>'
    );
    $replace = array(
      '<p></p>',
      '<br />'
    );
    $content = str_ireplace($search, $replace, $content);
  }
  /**
   * Recursively walk the dom tree, skipping defined tags and working only on nodes of TEXT type
   *
   * @access protected
   * @param DOMElement &$node
   * @return void
   */
  protected function walkDom (&$node)
  {
    // make sure we are always working with lowercase tagname
    $this->config['excludeTags'] = strtolower($this->config['excludeTags']);
    if ($node->nodeName != 'a' && ! t3lib_div::inList($this->config['excludeTags'], $node->nodeName)) {
      if ($node->nodeType == XML_TEXT_NODE && strlen(trim($node->nodeValue)) >= 1) {
        $this->markToolTip($node);
      } else {
        $cNodes = $node->childNodes;
        if (count($cNodes) > 0) {
          foreach ($cNodes as $cNode)
            $this->walkDom($cNode);
        }
      }
    }
  }
  /**
   * Prepare regular expressions for all glossary words and call function to mark as glossary word on matches
   *
   * @access protected
   * @param DOMNode &$node	XML_TEXT_NODE to work on
   * @return void
   */
  protected function markToolTip (&$node)
  {
    $searchWords = $replaceWords = array();
    foreach (array_keys($this->glossaryEntries) as $glossaryWord) {
      $regexModifier = ($this->config['ignoreCase'] == true ? 'ei' : 'e');
      $searchWords[] = '#([.,:-_\s=]|\A)(' . preg_quote($glossaryWord) . ')([.,:-_\s=]|\Z)#' . $regexModifier;
      $replaceWords[] = '"$1" . $this->markGlossaryWord("$2") . "$3";';
    }
    $node->nodeValue = preg_replace($searchWords, $replaceWords, $node->nodeValue);
  }
  /**
   * Convert the DOMDocument back into an HTML string and transform all markers
   *
   * @access protected
   * @param DOMDocument $domObject
   * @param DOMElement $root
   * @return string
   */
  protected function createToolTips (&$domObject, &$root, $encoding = null)
  {
    // convert DOMdocument back to string and strip the outer div we added
    $content = substr($domObject->saveXML($root, LIBXML_NOEMPTYTAG), 5, - 6);
    // if the input was UTF-8 encoded we now have a double encoding (know bug in saveXML())
    #        $content = preg_replace('#<br\s*\/>..<br\s*\/>#U','<br/><br/>', $content);
    #        if ($encoding == 'UTF-8')
    #            $content = utf8_decode($content);
    return preg_replace('/%%%(\d+)\|([^%]*)%%%/e', '$this->wrapGlossaryWord($1, "$2")', $content);
  }
  /**
   * Transform the markers into real HTML code since we have finished parsing the DOM
   *
   * @access protected
   * @param integer $uid	id of the glossary word to work on
   * @param string $source the text part to work on
   * @return string
   */
  protected function wrapGlossaryWord ($uid, $source)
  {
    $definition = $this->glossaryEntries[$this->glossaryIndex[$uid]];
    $linkParams = array('additionalParams'=>'&tx_gbglossary_main[controller]=Glossary&tx_gbglossary_main[action]=show&tx_gbglossary_main[definition]=' . intval($uid) , 'ATagParams'=>'class="gbglossary_csstooltip"' , 'useCacheHash'=>1);
    if (t3lib_utility_Math::canBeInterpretedAsInteger($this->config['detailsPid']) && intval($this->config['detailsPid'])) {
      $linkParams['parameter'] = intval($this->config['detailsPid']);
    } else {
      $linkParams['parameter'] = $GLOBALS['TSFE']->id;
      $linkParams['additionalParams'] = '';
    }
    return $this->cObj->typolink('<span class="dfn">' . $definition['longversion'] . '<br/>[Mehr: Klick auf den Begriff]</span>' . $source, $linkParams);
  }
  /**
   * Mark all glossary entries in the XML_TEXT_NODE without creating HTML code which gets escaped by DOM functions
   *
   * @access protected
   * @param string $word
   * @return string
   */
  protected function markGlossaryWord ($word)
  {
    $index = ($this->config['ignoreCase'] == true ? strtolower($word) : $word);
    $definition = $this->glossaryEntries[$index];
    return '%%%' . $definition['uid'] . '|' . $word . '%%%';
  }
  /**
   * Get all glossary words which are relevant to the current page
   *
   * @access protected
   * @return void
   */
  protected function getGlossaryWords ()
  {
    $pidList = array();
    if (! empty($this->config['pidList'])) {
      if (strtolower($this->config['pidList']) == 'pagetree') {
        if (count($GLOBALS['TSFE']->rootLine)) {
          foreach ($GLOBALS['TSFE']->rootLine as $rootPage) {
            $pidList[] = $rootPage['uid'];
          }
        }
      } else {
        $pidList = t3lib_div::intExplode(',', $this->config['pidList']);
      }
    }
    $andWhere = '';
    if (count($pidList))
      $andWhere = ' AND pid IN (' . join(',', $pidList) . ')';
    // limit to the requested language
    $andWhere .= ' AND tx_gbglossary_domain_model_definition.sys_language_uid = ' . intval($GLOBALS['TSFE']->sys_language_uid);
    // respect enableFields
    $andWhere .= ' ' . $this->cObj->enableFields('tx_gbglossary_domain_model_definition');
    $result = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('*', 'tx_gbglossary_domain_model_definition', 'tx_gbglossary_domain_model_definition.exclude = 0 ' . $andWhere, '', '');
    if (is_array($result) && count($result)) {
      foreach ($result as $definition) {
        $index = ($this->config['ignoreCase'] == true ? strtolower($definition['short']) : $definition['short']);
        $this->glossaryEntries[$index] = $definition;
        $this->glossaryIndex[$definition['uid']] = $index;
      }
    }
  }
}
