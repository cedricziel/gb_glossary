<?php
/**
 * Plugin Configuration
 */
Tx_Extbase_Utility_Extension::configurePlugin(
  $_EXTKEY,                                                                       // The name of the extension in UpperCamelCase
  'Main',                                                                         // A unique name of the plugin in UpperCamelCase
  array(                                                                          // An array holding the controller-action-combinations that are accessible
    'Glossary' => 'index,list,show'
  ),
  array(                                                                          // An array of non-cachable controller-action-combinations (they must already be enabled)
  )
);
