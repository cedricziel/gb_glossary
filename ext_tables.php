<?php
if (!defined ('TYPO3_MODE')) die ('Access denied.');

/**
 * Plugin Configuration
 */
Tx_Extbase_Utility_Extension::registerPlugin(
  $_EXTKEY,
  'Main',
  'Frontend Glossary Output'
);

/**
 * TypoScript Setup
 */
t3lib_extMgm::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Gute Botschafter Glossary');

$pluginSignature = str_replace('_','',$_EXTKEY) . '_main';
$TCA["tt_content"]["types"]["list"]["subtypes_excludelist"][$pluginSignature]="layout,select_key,recursive";

t3lib_extMgm::allowTableOnStandardPages('tx_gbglossary_domain_model_definition');
t3lib_extMgm::addToInsertRecords('tx_gbglossary_domain_model_definition');
$TCA['tx_gbglossary_domain_model_definition'] = array (
  'ctrl' => array (
    'title'                     => 'LLL:EXT:gb_glossary/Resources/Private/Language/locallang_db.xml:tx_gbglossary_domain_model_definition',
    'label'                     => 'short',
    "tstamp"                    => "tstamp",
    "crdate"                    => "crdate",
    "cruser_id"                 => "cruser_id",
    "versioning"                => "1",
    "languageField"             => "sys_language_uid",
    "transOrigPointerField"     => "l18n_parent",
    "transOrigDiffSourceField"  => "l18n_diffsource",
    "default_sortby"            => "ORDER BY short",
    "delete"                    => "deleted",
    "enablecolumns"       => Array (
      "disabled"  => "hidden",
      "starttime" => "starttime",
      "endtime"   => "endtime",
      "fe_group"  => "fe_group",
    ),
    'dynamicConfigFile'         => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/tca.php',
    'iconfile'                  => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Private/Icons/icon_tx_gbglossary_domain_model_definition.gif'
  ),
  "feInterface" => Array (
    "fe_admin_fieldList"        => "sys_language_uid, l18n_parent, l18n_diffsource, hidden, starttime, endtime, fe_group, short, shortcut, longversion, shorttype, description, link, exclude, website",
  )
);
