<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2009-2012 Gute Botschafter GmbH, Morton Jonuschat <m.jonuschat@gute-botschafter.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
/**
 * The Glossary controller for the gb_glossary package
 *
 * @author Morton Jonuschat <m.jonuschat@gute-botschafter.de>
 * @copyright Copyright (c) 2009-2012, Gute Botschafter GmbH, Morton Jonuschat
 * @version $Id:$
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */
class Tx_GbGlossary_Controller_GlossaryController extends Tx_Extbase_MVC_Controller_ActionController
{
  /**
   * @var Tx_GbGlossary_Domain_Model_DefinitionRepository
   */
  protected $definitionRepository;
  /**
   * Initializes the current action
   *
   * @return void
   */
  public function initializeAction ()
  {
    $this->definitionRepository = t3lib_div::makeInstance('Tx_GbGlossary_Domain_Repository_DefinitionRepository');
  }
  /**
   * Index action for this controller. Forwards control somewhere else
   *
   * @return string The rendered view
   */
  public function indexAction ()
  {
    $this->forward('list', 'Glossary');
  }
  /**
   * Show action for this controller. Displays glossary index list
   *
   * @return string The rendered view
   */
  public function listAction ()
  {
    $definitions = array();
    $letters = array();
    foreach ($this->definitionRepository->findAllOrdered() as $entry) {
      $word = mb_convert_encoding(strtoupper($entry->getShort()), 'HTML-ENTITIES', 'auto');
      $letter = substr($word,0,1);
      switch($letter) {
      case '&':
        $letter = substr($word,1,1);
      default:
        $definitions[$letter][$entry->getShort()] = $entry;
      }
    }
    for ($letter = 'A'; $letter != 'AA'; $letter ++) {
      if (empty($definitions[$letter]) || ! is_array($definitions[$letter]) || ! count($definitions[$letter])) {
        $letters[$letter] = '0';
      } else {
        $letters[$letter] = '1';
      }
    }
    $this->view->assign('definitions', $definitions);
    $this->view->assign('letters', $letters);
  }
  /**
   * Details action for this controller. Displays glossary entry details
   *
   * @param Tx_GbGlossary_Domain_Model_Definition $definition The Definition to show
   * @return string The rendered view
   */
  public function showAction (Tx_GbGlossary_Domain_Model_Definition $definition)
  {
    if (array_key_exists('backPid', $this->request->getArguments()))
      $this->view->assign('backPid', $this->request->getArgument('backPid'));
    $this->view->assign('definition', $definition);
  }
}
