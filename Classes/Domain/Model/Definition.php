<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2009-2012 Gute Botschafter GmbH, Morton Jonuschat <m.jonuschat@gute-botschafter.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
/**
 * The Glossary controller for the gb_glossary package
 *
 * @author Morton Jonuschat <m.jonuschat@gute-botschafter.de>
 * @copyright Copyright (c) 2009-2012, Gute Botschafter GmbH, Morton Jonuschat
 * @version $Id:$
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */
class Tx_GbGlossary_Domain_Model_Definition extends Tx_Extbase_DomainObject_AbstractEntity
{
  /**
   * @var string
   */
  protected $short = '';
  /**
   * @var string
   */
  protected $shortcut = '';
  /**
   * @var string
   */
  protected $longversion = '';
  /**
   * @var string
   */
  protected $shorttype = '';
  /**
   * @var string
   */
  protected $description = '';
  /**
   * @var integer
   */
  protected $exclude = '';
  /**
   * @var string
   */
  protected $website = '';
  /**
   * Get this definitions title
   *
   * @return string
   */
  public function getShort ()
  {
    return $this->short;
  }
  /**
   * Get this definitions type
   *
   * @return string
   */
  public function getShortCut ()
  {
    return $this->shortcut;
  }
  /**
   * Get this definitions longer explanation
   *
   * @return string
   */
  public function getLongVersion ()
  {
    return $this->longversion;
  }
  /**
   * Get this definitions type
   *
   * @return string
   */
  public function getShortType ()
  {
    return $this->shorttype;
  }
  /**
   * Get this definitions title
   *
   * @return string
   */
  public function getDescription ()
  {
    return $this->description;
  }
  /**
   * Get this definitions website
   *
   * @return string
   */
  public function getWebsite ()
  {
    return $this->website;
  }
}
