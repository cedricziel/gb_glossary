<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2009-2012 Gute Botschafter GmbH, Morton Jonuschat <m.jonuschat@gute-botschafter.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
/**
 * The Glossary controller for the gb_glossary package
 *
 * @author Morton Jonuschat <m.jonuschat@gute-botschafter.de>
 * @copyright Copyright (c) 2009-2012, Gute Botschafter GmbH, Morton Jonuschat
 * @version $Id:$
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */
class Tx_GbGlossary_Domain_Repository_DefinitionRepository extends Tx_Extbase_Persistence_Repository
{
  public function findAllOrdered ()
  {
    return $this->createQuery()->setOrderings(array('short'=>Tx_Extbase_Persistence_QueryInterface::ORDER_ASCENDING))->execute();
  }
}
